package main;

import java.util.ArrayList;

public class Account {

    private long accountNumber;
    private double currentBalance;
    private ArrayList<Transaction> transactions;

    public Account(long accountNumber, double currentBalance, ArrayList<Transaction> transactions) {
        this.accountNumber = accountNumber;
        this.currentBalance = currentBalance;
        this.transactions = transactions;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void deposit(double amount) {
        transactions.add(new Transaction(this.accountNumber, this.accountNumber, amount));
        currentBalance += amount;
    }

    public boolean withdraw(double amount) {
        if (amount > currentBalance) return false;
        transactions.add(new Transaction(this.accountNumber, this.accountNumber, amount * -1));
        currentBalance -= amount;
        return true;
    }

    public boolean transfer(long accountNumber, double amount) {
        if (accountNumber == this.accountNumber || amount > currentBalance) return false;
        transactions.add(new Transaction(this.accountNumber, accountNumber, amount * -1));
        currentBalance -= amount;
        return true;
    }
}