package main;

class Transaction {

    private long from;
    private long to;
    private double amount;
    private long date;

    public Transaction(long from, long to, double amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
        date = System.currentTimeMillis();
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public double getAmount() {
        return amount;
    }

    public long getDate() {
        return date;
    }
}
