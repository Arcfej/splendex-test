package main;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    private enum Command {
        current_balance,
        deposit,
        withdraw,
        transfer,
        history,
        save_and_exit
    }

    private static final String NEW_LINE = System.lineSeparator();
    private static final String LINE_SEPARATOR = "-----------------------------------------------------------" + NEW_LINE;
    private static final String COLUMN_WIDTH = "%-40s%s%n";

    private static Account account;

    public static void main(String[] args) {
        account = getAccount();
        getCommand();
    }

    private static Account getAccount() {
        // TODO read account from file
        return new Account(ThreadLocalRandom.current().nextLong(), 0, new ArrayList<>());
    }

    private static void getCommand() {
        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.println(NEW_LINE
                    + "Current balance: " + account.getCurrentBalance() + NEW_LINE
                    + LINE_SEPARATOR
                    + "Please enter a command: " + NEW_LINE
                    + LINE_SEPARATOR
                    + Command.current_balance + NEW_LINE
                    + Command.deposit.name() + " <number (0.00 format)>" + NEW_LINE
                    + Command.withdraw.name() + " <number (0.00 format)>" + NEW_LINE
                    + Command.transfer.name() + " <account number> <amount of money (0.00 format)>" + NEW_LINE
                    + Command.history.name() + NEW_LINE
                    + Command.save_and_exit.name() + NEW_LINE
            );
            String input = scan.nextLine();
            String[] words = input.split(" ");
            Command command = null;
            try {
                command = Command.valueOf(words[0]);
                switch (command) {
                    case current_balance:
                        System.out.println("Current ballance: " + account.getCurrentBalance());
                        break;
                    case deposit:
                        double deposit = Double.valueOf(words[1]);
                        if (deposit < 0) System.out.println("Failure!");
                        else {
                            account.deposit(deposit);
                            System.out.println("Success!");
                        }
                        break;
                    case withdraw:
                        double withdraw = Double.valueOf(words[1]);
                        if (withdraw < 0 || !account.withdraw(withdraw)) System.out.println("Failure!");
                        else System.out.println("Success!");
                        break;
                    case transfer:
                        long accountNumber = Long.valueOf(words[1]);
                        double amount = Double.valueOf(words[2]);
                        if (amount < 0 || !account.transfer(accountNumber, amount)) System.out.println("Failure!");
                        else System.out.println("Success!");
                        break;
                    case history:
                        printHistory();
                        break;
                    case save_and_exit:
                        saveAccount();
                        System.exit(0);
                        break;
                }
            } catch (Exception e) {
                System.out.print("Error: Not a valid command" + NEW_LINE + LINE_SEPARATOR);
            }
        }
    }

    private static void printHistory() {
        System.out.printf(COLUMN_WIDTH, "FROM", "TO", "AMOUNT", "DATE");
        for (Transaction transaction : account.getTransactions()) {
            System.out.printf(COLUMN_WIDTH,
                    transaction.getFrom(), transaction.getTo(), transaction.getAmount(), transaction.getDate());
        }
    }

    private static void saveAccount() {
        // TODO save account to file
    }
}
// 22